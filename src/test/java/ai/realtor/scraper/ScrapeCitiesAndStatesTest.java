package ai.realtor.scraper;

import ai.realtor.models.Agent;
import ai.realtor.utilities.DriverConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ScrapeCitiesAndStatesTest extends DriverConfig {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private String BaseURL;
    private String City;
    private String State;

    @BeforeSuite
    public void before() {
        BaseURL = System.getProperty("inputParameters.baseURL", "https://www.realtor.com/realestateagents/");
        City = System.getProperty("inputParameters.city", "warsaw").toLowerCase();
        State = System.getProperty("inputParameters.state", "in").toLowerCase();
    }

    @Test
    public void getRealtors() {
        String baseTargetURL = BaseURL + City + "_" + State;
        System.out.println("baseTargetURL: " + baseTargetURL);

        int totalPages = getTotalPages(baseTargetURL);
        System.out.println("totalPages: " + totalPages);

        List<Agent> realtors = new ArrayList<Agent>();
        String targetURL;

        for (int page = 0; page < totalPages; page++) {
            targetURL = baseTargetURL + "/pg-" + page;
            System.out.println("targetURL: " + targetURL);

            driver.navigate().to(targetURL);

            try {
                this.takeScreenshot("realtor-page-" + page + ".png");
            } catch (IOException ignored) {
            }

            List<WebElement> contactInfoElements = driver.findElements(By.xpath("//a[@id='call_inquiry_cta']"));
            System.out.println("contactInfoElements size: " + contactInfoElements.size());

            for (WebElement contactInfo : contactInfoElements) {
                String cityState = contactInfo.getAttribute("data-agent-address");
                System.out.println("cityState: " + cityState);
                Agent agent = new Agent(
                        contactInfo.getAttribute("data-agent-name"),
                        contactInfo.getAttribute("href").replace("tel:", ""),
                        cityState.split(",")[0],
                        cityState.split(",")[1].trim()
                );
                System.out.println("agent: " + agent);
                realtors.add(agent);
            }
        }

        String agents = gson.toJson(realtors);

        try {
            this.saveFile("agents.json", agents);
        } catch (IOException ex) {
            ex.printStackTrace();

            System.out.println("Could not save agents to json file");
        }

        System.setProperty("output", String.format("{\"result\": %s}", agents));

        System.out.println("output: " + gson.fromJson(System.getProperty("output"), Map.class));
    }

    private int getTotalPages(String url) {
        driver.navigate().to(url);
        List<WebElement> paginationElements = driver.findElements(By.xpath("//a[@data-pjax='pjax']"));
        Collections.reverse(paginationElements);

        return Integer.parseInt(paginationElements.get(1).getText().trim());
    }
}
