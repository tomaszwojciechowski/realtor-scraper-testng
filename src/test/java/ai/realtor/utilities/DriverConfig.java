package ai.realtor.utilities;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class DriverConfig {

    public static WebDriver driver;
    public static String artifactsPath = System.getProperty("ARTIFACTS_PATH", "./tmp/artifacts");
    public static String screenshotsPath = System.getProperty("SCREENSHOTS_PATH", "./tmp/screenshots");

    @BeforeSuite
    public void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
    }

    @AfterSuite
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void takeScreenshot(String fileName) throws IOException {
        TakesScreenshot scrShot = ((TakesScreenshot) driver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile = new File(screenshotsPath + "/" + fileName);
        FileUtils.copyFile(SrcFile, DestFile);
    }

    public void saveFile(String fileName, String contents) throws IOException {
        FileWriter fileWriter = new FileWriter(artifactsPath + "/" + fileName);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(contents);
        printWriter.close();
    }
}
